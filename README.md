﻿This was written by a teammate from my College Capstone Project, we used Unity in that but we're, obviously, using GameMaker
so if you see Unity refrences replace it with GameMaker.

* * *

## Instructions ##

### Cloning ###

The first thing you need to do is clone the entire repository to your local PC.

* At the home page of the repository, near the top left underneath the Overview, you will find a small button with a download icon.
* Click this to open a drop-down menu, and then click Clone in SourceTree.
* This will prompt SourceTree to open, and ask you how you'd like to configure the repository.
	* The only thing you should change is the Directory. Make sure it points to a folder you have easy access to, titled appropriately.

The next step is _very important_. 
Every time you clone the entire repository, __always__ clone the Working Copy Branch. This can be accomplished as follows:

* Near the bottom left, click Advanced Options.
* You will see a drop-down menu titled "Checkout Branch". Select the branched titled "WorkingCopyBranch".
* From here, SourceTree will start you off in the branch you selected.

__*Do not check out the `Master` branch, and do not pull from or push to the `Master` branch unless instructed otherwise.*__

### Branches ###

To access your personal branch of the repository, follow these steps.

* Navigate to the "Branches" page, located on the left of the screen while browsing this repository.
* On this page, you will see all available branches in the repository.
	* At the top left, you can filter by "active" and "merged". If you don't see your branch, switch between the filters to find it.
* Click the branch assigned to you.
* Near the top right of the page, you will see a drop-down menu to "check out" your copy of the branch.
	* If clicking the button doesn't work, you can manually copy-paste the string given to you in SourceTree terminal.
* This will prompt SourceTree to open a dialog asking you to clarify some options. You shouldn't need to worry about any of this, and can go ahead and hit OK/Checkout.

If all goes well and good, you should have your person branch checked out into your SourceTree.
You should end up with the `WorkingCopyBranch`, and your own assigned branch.

Should you run into any issues, use this video for quick reference.
[SourceTree Crash Course](https://www.youtube.com/watch?v=GVSYgxECm8o)

### Pushing and Pulling ###

While you're working in SourceTree, make sure your active branch (the branch you do work in) is _always_ your personal branch.

It is vital to maintain the workflow of the project. To do this, you must adhere strictly to the following steps _in order_:

1. Ensure that you have both WorkingCopyBranch and your personal branch.
2. BEFORE you begin working, change your active branch to WorkingCopyBranch.
3. PULL from the WorkingCopyBranch to get the latest version of the project.
4. MERGE the WorkingCopyBranch into your personal branch.
5. Change your active branch to your personal branch.

That will get you started, and ready to do some work.
When you're finished, follow these steps _in order_:

1. PUSH your changes to your personal branch.
2. Change your active branch to WorkingCopyBranch.
3. PULL WorkingCopyBranch to make sure you are still up to date.
4. MERGE your personal branch with WorkingCopyBranch.
5. PUSH the changes to WorkingCopyBranch.

If you follow these instructions carefully, and make sure not to work on anything anyone else is currently working on, this will work seamlessly.

__*Take note:*__ Any time you get up, walk away from your computer to get a snack/take a break/feed your dog, you are considered FINISHED.
Perform the latter 5 steps ANY time you leave your work station for ANY reason (unless your house is actively burning down).

* * *

### Contribution guidelines ###

For this project, SourceTree and the repository in general will be held in tight reins, and to higher standards.

Absolutely **_do not_** pull from or push to the `master` branch. This will only be updated by the repository creator, at the time that it needs to be updated (4 time total over a period of 4 months).

Make absolute certain that the work you do, and the files you manipulate, are not being worked on/manipulated by anyone else. This will lead to merge conflicts.
If your commit contains any merge conflicts, it will be rejected.

Do not alter the hierarchy of the GameMaker project. The folders are created to maintain consistency, and to ensure all team members are on the same page at all times.